-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 15/03/2020 às 13:31
-- Versão do servidor: 10.1.13-MariaDB
-- Versão do PHP: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `doador`
--
CREATE DATABASE IF NOT EXISTS `doador` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `doador`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cadastro`
--

CREATE TABLE `cadastro` (
  `id` int(5) NOT NULL,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `cpf` varchar(14) COLLATE utf8_bin NOT NULL,
  `tel1` varchar(14) COLLATE utf8_bin NOT NULL,
  `tel2` varchar(14) COLLATE utf8_bin DEFAULT NULL,
  `nasc` date NOT NULL,
  `doacao` int(5) NOT NULL,
  `valor` decimal(5,2) NOT NULL,
  `pgto` int(5) NOT NULL,
  `rua` varchar(40) COLLATE utf8_bin NOT NULL,
  `nro` smallint(6) NOT NULL,
  `compl` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `bairro` varchar(40) COLLATE utf8_bin NOT NULL,
  `cidade` varchar(40) COLLATE utf8_bin NOT NULL,
  `estado` varchar(2) COLLATE utf8_bin NOT NULL,
  `cadastro` int(11) NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `cadastro`
--

INSERT INTO `cadastro` (`id`, `nome`, `email`, `cpf`, `tel1`, `tel2`, `nasc`, `doacao`, `valor`, `pgto`, `rua`, `nro`, `compl`, `bairro`, `cidade`, `estado`, `cadastro`, `ativo`) VALUES
(1, 'José da Silva', 'jose.silva@silva.br', '123.456.789-09', '19 9 2492-8420', NULL, '1995-04-11', 4, '85.90', 1, 'Pinheiros', 75, 'casa 03', 'Parque Primavera', 'Campinas', 'SP', 1584242691, 1),
(2, 'Carlos Daniel', 'daniel.carlos@gmail.com', '111.111.111-11', '35 9 2348-2734', NULL, '2001-09-24', 1, '35.00', 2, 'Gertrudes Venâncio', 540, NULL, 'São Bento', 'Poços de Caldas', 'MG', 1584288186, 0),
(3, 'Ana Célia Ferreira', 'aninhaferreira@uol.com.br', '222.222.222-22', '21 9 2838-4723', NULL, '1996-02-29', 2, '25.00', 1, 'Luis Carlos Pace', 1325, 'apto 301', 'Vila Madalena', 'Rio de Janeiro', 'RJ', 1584288362, 1),
(4, 'Jessica Daniela', 'jessi_k@live.com', '444.444.444-44', '19 9 2637-1263', '19 9 2726-3746', '2001-10-29', 3, '399.90', 1, 'Luiza Erundina', 400, 'casa 03', 'Jd. América', 'Campinas', 'SP', 1584288619, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `doacao`
--

CREATE TABLE `doacao` (
  `id` int(5) NOT NULL,
  `nome` varchar(20) COLLATE utf8_bin NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `doacao`
--

INSERT INTO `doacao` (`id`, `nome`, `ativo`) VALUES
(1, 'Anual', 1),
(2, 'Bimestral', 1),
(3, 'Única', 1),
(4, 'Semestral', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `frmPgto`
--

CREATE TABLE `frmPgto` (
  `id` int(5) NOT NULL,
  `nome` varchar(20) COLLATE utf8_bin NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `frmPgto`
--

INSERT INTO `frmPgto` (`id`, `nome`, `ativo`) VALUES
(1, 'Débito', 1),
(2, 'Crédito', 1);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `cadastro`
--
ALTER TABLE `cadastro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_doacao` (`doacao`),
  ADD KEY `fk_pgto` (`pgto`);

--
-- Índices de tabela `doacao`
--
ALTER TABLE `doacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `frmPgto`
--
ALTER TABLE `frmPgto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `cadastro`
--
ALTER TABLE `cadastro`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `doacao`
--
ALTER TABLE `doacao`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `frmPgto`
--
ALTER TABLE `frmPgto`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `cadastro`
--
ALTER TABLE `cadastro`
  ADD CONSTRAINT `FKdoacao` FOREIGN KEY (`doacao`) REFERENCES `doacao` (`id`),
  ADD CONSTRAINT `FKpgto` FOREIGN KEY (`pgto`) REFERENCES `frmPgto` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
