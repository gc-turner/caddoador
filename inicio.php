<div class="row">
    <div class="col-xs-12 col-md-10 col-md-offset-1">
        <div class="panel panel-default">  
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="doacao">Intervalo da doação</label>
                        <select class="form-control" id="doacao" name="doacao">
                            <option value=""></option>
                            <?php
                            $sql = "SELECT id, nome FROM doacao WHERE ativo = 1 ORDER BY nome";
                            $result = $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
                            while($linha = $result->fetch()) {
                                echo "<option value={$linha['id']}>{$linha['nome']}</option>";
                            }
                            ?>
                        </select>
                     </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="pgto">Forma de pagamento</label>
                        <select class="form-control" id="pgto" name="pgto">
                            <option value=""></option>
                            <?php
                            $sql = "SELECT id, nome FROM frmPgto WHERE ativo = 1 ORDER BY nome";
                            $result = $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
                            while($linha = $result->fetch()) {
                                echo "<option value={$linha['id']}>{$linha['nome']}</option>";
                            }
                            ?>
                        </select>
                     </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label for="valorMin">Valor Mín.</label>
                        <input class="form-control" type="text" maxlength="15" id="valorMin" name="valorMin" value="" onkeypress="moeda(this, event);" />
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label for="valorMax">Valor Máx.</label>
                        <input class="form-control" type="text" maxlength="15" id="valorMax" name="valorMax" value="" onkeypress="moeda(this, event);" />
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="cidade">Cidade</label>
                        <select class="form-control" id="cidade" name="cidade">
                            <option value=""></option>
                            <?php
                            $sql = "SELECT DISTINCT(cidade) FROM cadastro ORDER BY cidade";
                            $result = $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
                            while($linha = $result->fetch()) {
                                echo "<option value={$linha['cidade']}>{$linha['cidade']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <label for="estado">Estado</label>
                        <div id="selEstado">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="button" id="btnAdd" name="btnAdd" class="btn btn-success" onclick="direciona('doador')"><em class="fa fa-plus"></em> Adicionar</button>
                <button type="button" id="btnFind" name="btnFind" class="btn btn-primary pull-right" onclick="lstDoador();"><em class="fa fa-search"></em> Pesquisar</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-10 col-md-offset-1">
        <div class="panel panel-default">  
            <div class="panel-heading">
                <h3 class="panel-header">Lista de Doadores</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div id="lstDoador" name="lstDoador" class="col-xs-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<form role="form" id="frmDoadorCh" name="frmDoadorCh" method="post" action="./">
   <input type="hidden" id="page" name="page" value="doador" />
   <input type="hidden" id="id" name="id" value="" />
</form>

<script>
    $(document).ready(function(){
        $(".page-title > .title").html("Doadores");
        carregaEstado();
        lstDoador();
    });
</script>
