<?php
require_once("bd.php");

$arr = (array_merge($_POST, $_GET));
foreach($arr as $key => $value){
    eval("$" . $key . " = \"" . addslashes(str_replace("'", "", $value)) . "\";");
}

if(!isset($page))
    $page = "inicio";

function erro($msg, $pagina = "inicio"){
   if($pagina == "inicio")
      $botao = "Página inicial";
   else
      $botao = "Prosseguir";
   
   echo "
      <div class='row'>
         <div class='col-xs-12 col-md-6 col-md-offset-3'>
            <div class='panel panel-danger'>
               <div class='panel-heading'>
                  <p class='panel-title'><em class=''></em> Não disponível!</p>
               </div>
               <div class='panel-body'>
                  <div class='row'>
                     <div class='col-xs-12'>
                        <p>$msg</p>
                     </div>
                  </div>
               </div>
               <div class='panel-footer'>
                  <button type='button' class='btn btn-primary' onclick='direciona(\"$pagina\");'>$botao</button>
               </div>
            </div>
         </div>
      </div>
   ";
   exit;
}
?>