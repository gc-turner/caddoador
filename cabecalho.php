<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title>Cadastro de Doadores</title>
        
        <meta name="keywords" content="Cadastro de Doadores" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- CSS -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css" />

        <!-- Font-Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Local -->
        <link rel="stylesheet" href="css/doador.css" />

        <!-- Javascript -->
        <!-- jQuery -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap/bootstrap.min.js"></script>

        <!-- Local -->
        <script src="js/doador.js"></script>

        <?php
        require_once("config.php");
        ?>
