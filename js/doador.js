"use strict";

$(document).ready(function(){
   $("[data-toggle='tooltip']").tooltip({
      html: true,
      placement: "auto top"
   });
});

function initControls($parent){
   $parent.find('[data-toggle="tooltip"]').tooltip();
}

function direciona(pagina){
   $("#frmPage #page").val(pagina);
   $("#frmPage").submit();
}

function validaDoador(){   
   if($("#frmDoador #nome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   if($("#frmDoador #email").val() == ""){
      alert('O campo Email deve ser preenchido.');
      return;
   }
   if($("#frmDoador #cpf").val() == ""){
      alert('O campo CPF deve ser preenchido.');
      return;
   }
   if($("#frmDoador #tel1").val() == ""){
      alert('Os campos referentes ao Telefone 1 devem ser preenchidos.');
      return;
   }
   if($("#frmDoador #nascD").val() == ""){
      alert('Selecione o dia de nascimento.');
      return;
   }
   if($("#frmDoador #nascM").val() == ""){
      alert('Selecione o mês de nascimento.');
      return;
   }
   if($("#frmDoador #nascA").val() == ""){
      alert('Selecione o ano de nascimento.');
      return;
   }
   if($("#frmDoador #doacao").val() == ""){
      alert('Selecione o intervalo da doação.');
      return;
   }
   if($("#frmDoador #valor").val() == ""){
      alert('O campo Valor deve ser preenchido.');
      return;
   }
   if($("#frmDoador #pgto").val() == ""){
      alert('Selecione a forma de pagamento.');
      return;
   }
   if($("#frmDoador #rua").val() == ""){
      alert('O campo Rua deve ser preenchido.');
      return;
   }
   if($("#frmDoador #nro").val() == ""){
      alert('O campo Número deve ser preenchido.');
      return;
   }
   if($("#frmDoador #bairro").val() == ""){
      alert('O campo Bairro deve ser preenchido.');
      return;
   }
   if($("#frmDoador #cidade").val() == ""){
      alert('O campo Cidade deve ser preenchido.');
      return;
   }
   if($("#frmDoador #estado").val() == ""){
      alert('O campo Estado deve ser preenchido.');
      return;
   }
   if(!validaEmail($("#frmDoador #email").val())){
      alert('Informe um Email válido.');
      return; 
   }
   if(!validaCPF($("#frmDoador #cpf").val())){
      alert('Informe um CPF válido.');
      return; 
   }
   if(!validaTel($("#frmDoador #tel1DDD").val(), $("#frmDoador #tel1").val())){
      alert('Informe um Telefone válido.');
      return; 
   }
   if(($("#frmDoador #tel2DDD").val() !== "" && $("#frmDoador #tel2").val() !== "") && !validaTel($("#frmDoador #tel2DDD").val(), $("#frmDoador #tel2").val())){
      alert('Informe um Telefone válido.');
      return; 
   }
   if(!validaNasc($("#frmDoador #nascD").val(), $("#frmDoador #nascM").val(), $("#frmDoador #nascA").val())){
      alert('Informe uma data de Nascimento válida.');
      return; 
   }
   if(!$("#frmDoador #cpf").attr("disabled")){
      $.ajax({
         url: "doadorAJAX.php",
         data: "funcao=consultaDoador&cpf=" + $("#frmDoador #cpf").val(),
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            $("#frmDoador").submit();   
         }
      });
   } else {
       $("#frmDoador").submit();
   }
}

function editDoador(id){
   $("#frmDoadorCh #id").val(id);
   $("#frmDoadorCh").submit();
}

function delDoador(id){
   if(confirm("Deseja realmente excluir este doador?")){
      $.ajax({
         url: "doadorAJAX.php",
         data: "funcao=excluirDoador&id=" + id,
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            alert("Doador excluído.");
            direciona("inicio");
         }
      });
   }
   return;
}

function lstDoador(){
    var doacao, pgto, vMin, vMax, cidade, estado;
    doacao = $("#doacao").val();
    pgto = $("#pgto").val();
    vMin = $("#valorMin").val();
    vMax = $("#valorMax").val();
    cidade = $("#cidade").val();
    estado = $("#estado").val();
    $.ajax({
        url: "doadorAJAX.php",
        data: "funcao=lstDoador&doacao=" + doacao + "&pgto=" + pgto + "&vMin=" + vMin + "&vMax=" + vMax + "&cidade=" + cidade + "&estado=" + estado,
        method: "POST",
        success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
                alert(retorno.retorno + "!\n" + retorno.msg);
                return;
            }
            var html = "<table class='table table-result table-bordered table-striped table-hover table-responsive'><thead><tr><th>Nome</th><th>Email</th><th>CPF</th><th>Idade</th><th>Ações</th></tr></thead><tbody>";
            var results = retorno.msg;
            if(results.length > 0)
                for(let result of results){
                    html += "<tr><td>" + result.nome + "</a></td><td>" + result.email + "</td><td>" 
                            + result.cpf + "</td><td class='text-center'>" + result.idade 
                            + "</td><td><button type='button' class='btn btn-xs btn-danger' onclick='delDoador(\"" + result.id +"\")' data-toggle='tooltip' title='Remover doador'><em class='fa fa-trash'></em></button>"
                    +"<button type='button' class='btn btn-xs btn-warning' onclick='editDoador(\"" + result.id +"\")' data-toggle='tooltip' title='Editar doador'><em class='fa fa-edit'></em></button></td></tr>";
               }
            else
                html += "<tr><td colspan='5'>Nenhum doador cadastrado</td></tr>";
            html += "</tbody></table>";
            $("#lstDoador").html(html);
            initControls($("#resultado"));
        }
    });
}

function iniciaCampos(){
    carregaDia();
    carregaMes();
    carregaAno();
    carregaEstado();
}

function carregaDia(){
    var html = "<option value=''></option>";
    for(let i = 1; i < 32; i++){
        html += "<option value='" + (i < 10 ? ("0" + i) : i) + "'>" + (i < 10 ? ("0" + i) : i) + "</option>";
    }
    html = "<select class='form-control' id='nascD' name='nascD'>" + html + "</select>";
    
    $("#selDia").html(html);
}

function carregaMes(){
    var html = "<option value=''></option>";
    var meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    for(let i = 0; i < meses.length; i++){
        html += "<option value='" + (i < 10 ? ("0" + (i+1)) : (i + 1)) + "'>" + meses[i] + "</option>";
    }
    html = "<select class='form-control' id='nascM' name='nascM'>" + html + "</select>";
    
    $("#selMes").html(html);
}

function carregaAno(){
    var html = "<option value=''></option>";
    var hoje = new Date();
    for(let i = hoje.getFullYear(); i >= 1900; i--){
        html += "<option value='" + i + "'>" + i + "</option>";
    }
    html = "<select class='form-control' id='nascA' name='nascA'>" + html + "</select>";
    
    $("#selAno").html(html);
}

function carregaEstado(){
    var html = "<option value=''></option>";
    var estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];
    var estadosNome = ['Acre', 'Alagoas', 'Amapá', 'Amazonas', 'Bahia', 'Ceará', 'Distrito Federal', 'Espírito Santo', 'Goiás', 'Maranhão', 'Mato Grosso', 'Mato Grosso do Sul', 'Minas Gerais', 'Pará', 'Paraíba', 'Paraná', 'Pernambuco', 'Piauí', 'Rio de Janeiro', 'Rio Grande do Norte', 'Rio Grande do Sul', 'Rondônia', 'Roraima', 'Santa Catarina', 'São Paulo', 'Sergipe', 'Tocantins'];
    for(let i = 0; i < estados.length; i++){
        html += "<option value='" + estados[i] + "'>" + estadosNome[i] + "</option>";
    }
    html = "<select class='form-control' id='estado' name='estado'>" + html + "</select>";
    
    $("#selEstado").html(html);
}

function validaEmail(email){
    var regexEmail = /^[a-z0-9][a-z0-9._-]*@[a-z0-9.-]+\.?[a-z]*$/;
    
    return regexEmail.test(email);
}

function validaCPF(cpf){
    var regexCPF = /^\d{3}[.]?\d{3}[.]?\d{3}[-]?\d{2}$/;
    var cpfAux, resto1, resto2, dig1, dig2;
    
    if(regexCPF.test(cpf)){
        cpfAux = cpf;
        cpfAux = cpfAux.replace(/\./g, "");
        cpfAux = cpfAux.replace(/\-/g, "");
        
        resto1 = ((10 * parseInt(cpfAux[0])) + (9 * parseInt(cpfAux[1])) + (8 * parseInt(cpfAux[2])) + (7 * parseInt(cpfAux[3])) + (6 * parseInt(cpfAux[4]))
                + (5 * parseInt(cpfAux[5])) + (4 * parseInt(cpfAux[6])) + (3 * parseInt(cpfAux[7])) + (2 * parseInt(cpfAux[8]))) % 11;
        dig1 = resto1 < 2 ? 0 : (11 - resto1);
        
        resto2 = ((11 * parseInt(cpfAux[0])) + (10 * parseInt(cpfAux[1])) + (9 * parseInt(cpfAux[2])) + (8 * parseInt(cpfAux[3])) + (7 * parseInt(cpfAux[4]))
                + (6 * parseInt(cpfAux[5])) + (5 * parseInt(cpfAux[6])) + (4 * parseInt(cpfAux[7])) + (3 * parseInt(cpfAux[8])) + (2 * parseInt(cpfAux[9]))) % 11;
        dig2 = resto2 < 2 ? 0 : (11 - resto2);
        
        if(dig1 == parseInt(cpfAux[9]) && dig2 == parseInt(cpfAux[10]))
            return true;
        
        return false;
    }
    return false;
}

function validaTel(ddd, tel){
    var regexTel = /^\d{2}?[ ]?9?[ ]?\d{4}[-]?\d{4}$/;
    
    return regexTel.test(ddd + " " + tel);
}

function validaNasc(dia, mes, ano){
    if(parseInt(dia) == 31 && (parseInt(mes) == 2 || parseInt(mes) == 4 || parseInt(mes) == 6 || parseInt(mes) == 9 || parseInt(mes) == 11))
        return false;
    if(parseInt(dia) == 30 && parseInt(mes) == 2)
        return false;
    if(!(((parseInt(ano) % 4 == 0) && !(parseInt(ano) % 100 == 0)) || (parseInt(ano) % 400 == 0)) && parseInt(dia) == 29 && parseInt(mes) == 2)
        return false;
    return true;
}

function moeda(obj, event) {
    var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var keychar = String.fromCharCode(key);
    var ponto = $(obj).val().indexOf(",");
    var cent = 0
    if (ponto !== -1) {
        cent = $(obj).val().substring(ponto + 1).length;
    }

    if (event.preventDefault) { //Firefox, Opera
        if (ponto !== -1 && key == 44) {
            event.preventDefault();
            return false;
        }
        if (key == 44 && $(obj).val().length == 0) {
            event.preventDefault();
            return false;
        }
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27) || (key == 44)) {
            event.stopPropagation();
        }
        else if ((("0123456789").indexOf(keychar) > -1)){
            if (cent == 2 && $(obj)[0].selectionEnd > ponto){
                event.preventDefault();
                return false;
            }
            else
                event.stopPropagation();
        }
        else {
            event.preventDefault();
            return false;
        }
    }
    else { //IE
        if (ponto !== -1 && key == 44) {
            event.returnValue = false;
        }
        if (key == 44 && $(obj).val().length == 0) {
            event.returnValue = false;
        }
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27) || (key == 44)) {
            event.returnValue = true;
        }
        else if ((("0123456789").indexOf(keychar) > -1)) {
            if (cent == 2 && $(obj)[0].selectionEnd > ponto){
                event.returnValue = false;
            }
            else
                event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }
}

function soNro(event) {
    var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var keychar = String.fromCharCode(key);
    if (event.preventDefault) { //Firefox, Opera
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
            event.stopPropagation();
        }
        else if ((("0123456789").indexOf(keychar) > -1))
            event.stopPropagation();
        else {
            event.preventDefault();
            return false;
        }
    }
    else { //IE
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
            event.returnValue = true;
        }
        else if ((("0123456789").indexOf(keychar) > -1)) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }
}

function tel(obj, event) {
    var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var keychar = String.fromCharCode(key);

    if (event.preventDefault) { //Firefox, Opera
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
            event.stopPropagation();
        }
        else if ((("0123456789").indexOf(keychar) > -1)){
            if($(obj).val() == "9")
                $(obj).val($(obj).val() + " ");
            if($(obj).val().substr(0, 1) == "9" && $(obj).val().length === 6)
                $(obj).val($(obj).val() + "-");
            if($(obj).val().substr(0, 1) != "9" && $(obj).val().length === 4)
                $(obj).val($(obj).val() + "-");
            if($(obj).val().substr(0, 1) != "9" && $(obj).val().length === 9) {
                event.preventDefault();
                return false;
            }

            event.stopPropagation();
        }
        else {
            event.preventDefault();
            return false;
        }
    }
    else { //IE
        if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
            event.returnValue = true;
        }
        else if ((("0123456789").indexOf(keychar) > -1)) {
            if($(obj).val() == "9")
                $(obj).val($(obj).val() + " ");
            if($(obj).val().substr(0, 1) == "9" && $(obj).val().length === 6)
                $(obj).val($(obj).val() + "-");
            if($(obj).val().substr(0, 1) != "9" && $(obj).val().length === 4)
                $(obj).val($(obj).val() + "-");
            if($(obj).val().substr(0, 1) != "9" && $(obj).val().length === 9) {
                event.returnValue = false;
            }

            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }
}