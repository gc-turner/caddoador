<?php
require_once("config.php");

switch($funcao){
    case "consultaDoador":
        $sql = "SELECT COUNT(*) FROM cadastro WHERE cpf = '$cpf'";
        $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
        $linha = $result->fetch();
        if(intval($linha[0]) ==  0)
            echo json_encode(array("retorno" => "OK"));
        else
            echo json_encode(array("retorno" => "Doador duplicado", "msg" => "Já existe um doador com este CPF."));
        break;
    case "excluirDoador":
        $sql = "UPDATE cadastro SET ativo = '0' WHERE id = '$id'";
        $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível excluir o doador.")));
        echo json_encode(array("retorno" => "OK"));
        break;
    case "lstDoador":
        $vMin = str_replace(",", ".", $vMin);
        $vMax = str_replace(",", ".", $vMax);
        
        $sqlCond = "";
        
        $sqlCond .= ($doacao == "" ? "" : " AND doacao = '$doacao'");
        $sqlCond .= ($pgto == "" ? "" : " AND pgto = '$pgto'");
        $sqlCond .= ($vMin == "" ? "" : " AND valor >= $vMin");
        $sqlCond .= ($vMax == "" ? "" : " AND valor <= $vMax");
        $sqlCond .= ($ciade == "" ? "" : " AND cidade = '$cidade'");
        $sqlCond .= ($estado == "" ? "" : " AND estado = '$estado'");
        
        $sql = "SELECT id, nome, email, cpf, nasc FROM cadastro WHERE ativo = 1$sqlCond ORDER BY nome";
        $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
        if($linha = $result->fetch()) {
            $dados = array();
            while($linha){
                $atD = intval(date("d"));
                $atM = intval(date("m"));
                $atA = intval(date("Y"));
                $nasc = explode("-", $linha['nasc']);
                $nD = intval($nasc[2]);
                $nM = intval($nasc[1]);
                $nA = intval($nasc[0]);
                
                $idade = ($nM > $atM || ($nD > $atD && $nM <= $atM)) ? ($atA - 1 - $nA) : ($atA - $nA);
                
                $dados[] = array("id" => $linha['id'], "nome" => $linha['nome'], "email" => $linha['email'], "cpf" => $linha['cpf'], 
                                 "idade" => $idade);
                
                $linha = $result->fetch();
            }
            echo json_encode(array("retorno" => "OK", "msg" => $dados));
        } else {
            echo json_encode(array("retorno" => "OK", "msg" => $result->rowCount()));
        }
        break;
}
?>