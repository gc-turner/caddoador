<div class="row">
    <div class="col-xs-12">
        <form role="form" class="panel panel-default" method="post" action="doador_ins.php" id="frmDoador" name="frmDoador">
            <input type="hidden" id="id" name="id" value="" />
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="nome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="50" id="nome" name="nome" value="" />
                    </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="email">Email <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="50" id="email" name="email" value="" />
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="cpf">CPF <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="14" id="cpf" name="cpf" placeholder="000.000.000-00" />
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="tel1">Telefone 1 <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <div class="simple col-xs-12" style="padding:0px">
                            <div class="col-xs-4 col-sm-3" style="padding:0px;padding-right:10px">
                                <input class="form-control" type="text" maxlength="2" id="tel1DDD" name="tel1DDD" placeholder="DDD" onkeypress="soNro(event);" />
                            </div>
                            <div class="col-xs-8 col-sm-9" style="padding:0px;padding-left:10px">
                                <input class="form-control" type="text" maxlength="11" id="tel1" name="tel1" placeholder="9 9999-9999" onkeypress="tel(this, event);" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="tel2">Telefone 2</label>
                        <div class="simple col-xs-12" style="padding:0px">
                            <div class="col-xs-4 col-sm-3" style="padding:0px;padding-right:10px">
                                <input class="form-control" type="text" maxlength="2" id="tel2DDD" name="tel2DDD" placeholder="DDD" onkeypress="soNro(event);" />
                            </div>
                            <div class="col-xs-8 col-sm-9" style="padding:0px;padding-left:10px">
                                <input class="form-control" type="text" maxlength="11" id="tel2" name="tel2" placeholder="9 9999-9999" onkeypress="tel(this, event);" />
                            </div>
                        </div>
                     </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="nasc">Nascimento <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <div class="simple col-xs-12" style="padding:0px">
                            <div class="col-sm-3" id="selDia">
                            </div>
                            <div class="col-sm-6" id="selMes">
                            </div>
                            <div class="col-sm-3" id="selAno">
                            </div>
                        </div>
                     </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="doacao">Intervalo da doação <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <select class="form-control" id="doacao" name="doacao">
                            <option value=""></option>
                           <?php
                            $sql = "SELECT id, nome FROM doacao WHERE ativo = 1 ORDER BY nome";
                            $result = $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
                            while($linha = $result->fetch()) {
                                echo "<option value={$linha['id']}>{$linha['nome']}</option>";
                            }
                            ?>
                        </select>
                     </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <label for="valor">Valor <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="15" id="valor" name="valor" value="" onkeypress="moeda(this, event);" />
                     </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="pgto">Forma de pagamento <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <select class="form-control" id="pgto" name="pgto">
                            <option value=""></option>
                            <?php
                            $sql = "SELECT id, nome FROM frmPgto WHERE ativo = 1 ORDER BY nome";
                            $result = $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
                            while($linha = $result->fetch()) {
                                echo "<option value={$linha['id']}>{$linha['nome']}</option>";
                            }
                            ?>
                        </select>
                     </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="rua">Rua <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="40" id="rua" name="rua" value="" />
                     </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <label for="nro">Nº <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="4" id="nro" name="nro" value="" onkeypress="soNro(event);" />
                     </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="compl">Complemento</label>
                        <input class="form-control" type="text" maxlength="20" id="compl" name="compl" value="" />
                    </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="bairro">Bairro <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="40" id="bairro" name="bairro" value="" />
                    </div>
                    <div class="form-group col-xs-12 col-md-8">
                        <label for="cidade">Cidade <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="40" id="cidade" name="cidade" value="" />
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="estado">Estado <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <div id="selEstado">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="button" class="btn btn-warning" onclick="direciona('inicio');">Voltar</button>
                <button type="button" class="btn btn-success" onclick="validaDoador();">Gravar</button>
            </div>
        </form>
    </div>
</div>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Cadastro de Doadores");
      iniciaCampos();
      $("#cpf").keypress(function(event) {
            var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            var keychar = String.fromCharCode(key);

            if (event.preventDefault) { //Firefox, Opera
                if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
                    event.stopPropagation();
                }
                else if ((("0123456789").indexOf(keychar) > -1)){
                    if($("#cpf").val().length === 3)
                        $("#cpf").val($("#cpf").val() + ".");
                    if($("#cpf").val().length === 7)
                        $("#cpf").val($("#cpf").val() + ".");
                    if($("#cpf").val().length === 11)
                        $("#cpf").val($("#cpf").val() + "-");
                    
                    event.stopPropagation();
                }
                else {
                    event.preventDefault();
                    return false;
                }
            }
            else { //IE
                if ((key == null) || (key == 0) || (key == 8) || (key == 13) || (key == 27)) {
                    event.returnValue = true;
                }
                else if ((("0123456789").indexOf(keychar) > -1)) {
                    if($("#cpf").val().length === 3)
                        $("#cpf").val($("#cpf").val() + ".");
                    if($("#cpf").val().length === 7)
                        $("#cpf").val($("#cpf").val() + ".");
                    if($("#cpf").val().length === 11)
                        $("#cpf").val($("#cpf").val() + "-");
                    
                    event.returnValue = true;
                }
                else {
                    event.returnValue = false;
                }
            }
        });
       <?php if(isset($id) && $id != "") {
       $sql = "SELECT * FROM cadastro WHERE id = '$id'";
       $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
       if($linha = $result->fetch()) {
           $nome = $linha['nome'];
           $email = $linha['email'];
           $cpf = $linha['cpf'];
           $tel1 = $linha['tel1'];
           $tel2 = isset($linha['tel2']) ? $linha['tel2'] : "";
           $nasc = explode("-",$linha['nasc']);
           $doacao = $linha['doacao'];
           $valor = str_replace(".", ",", $linha['valor']);
           $pgto = $linha['pgto'];
           $rua = $linha['rua'];
           $nro = $linha['nro'];
           $compl = isset($linha['compl']) ? $linha['compl'] : "";
           $bairro = $linha['bairro'];
           $cidade = $linha['cidade'];
           $estado = $linha['estado'];
           ?>
               $("#id").val("<?= $id; ?>");
                $("#nome").val("<?= $nome; ?>");               
               $("#email").val("<?= $email; ?>");    
               $("#cpf").val("<?= $cpf; ?>");
               $("#cpf").attr("disabled", "disabled");
               $("#tel1DDD").val("<?= substr($tel1, 0, 2) ; ?>");
               $("#tel1").val("<?= substr($tel1, 3) ; ?>");
               $("#tel2DDD").val("<?= substr($tel2, 0, 2) ; ?>");
               $("#tel2").val("<?= substr($tel2, 3) ; ?>");
               $("#nascD").val("<?= $nasc[2]; ?>");
               $("#nascM").val("<?= $nasc[1]; ?>");
               $("#nascA").val("<?= $nasc[0]; ?>");
               $("#doacao").val("<?= $doacao; ?>");
               $("#valor").val("<?= $valor; ?>");
               $("#pgto").val("<?= $pgto; ?>");
               $("#rua").val("<?= $rua; ?>");
               $("#nro").val("<?= $nro; ?>");
               $("#compl").val("<?= $compl; ?>");
               $("#bairro").val("<?= $bairro; ?>");
               $("#cidade").val("<?= $cidade; ?>");
               $("#estado").val("<?= $estado; ?>");
       <?php }
   } ?>
   });
</script>