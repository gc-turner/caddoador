<?php
require_once('cabecalho.php');
?>

   </head>
   <body>
        <form role="form" id="frmPage" name="frmPage" action="./" method="post">
            <input type="hidden" id="page" name="page" value="<?= $page; ?>" />
        </form>
        <header>
            <h1 class="main-title">Cadastro de Doadores</h1>
        </header>
        <div class="container">
            <aside class="content">
                    <section class="page-title col-xs-12">
                        <h2 class="title text-primary">&nbsp;</h2>
                    </section>
                    <section class="page-content">
                        <?php
                        if (file_exists("./$page.php"))
                           include("$page.php");
                        else
                           erro("A página informada não existe.");
                        ?>
                  </section>
            </aside>
        </div>
      <?php
         $bd = null;
      ?>
   </body>
</html>